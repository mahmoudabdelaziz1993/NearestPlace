# Nearest Governorate to apoint 
an Api has one endpoint to  search for the nearest Governorate in egypt to a point reference with latitude and longitude
## Installation
### first step 
Use the package manager [npm]
```bash
npm i 
```
### second step 
you need to have [.env] file with the following attributes
```bash
PORT=<port number of your choice>
DB_URL=< MongoDb url >
```
## Usage
run seeder for Db
```bash
npm run seed
```

to run the project on local machine on dev mode

```bash
npm run dev
```

## api Doc
postman documentation

[https://documenter.getpostman.com/view/6756867/SWLZeVHz?version=latest]





