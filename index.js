const app = require('express')()
if (process.env.NODE !== "production") {
    require("dotenv").config()
}
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const apiRouter = require("./routes/api");


app.use(bodyParser.json({extended: false }));
app.use("/api",apiRouter);

// app.get('/', async (req, res) => {
    
//     const data = await Place.find({
//         location:
//           { $near :
//              {
//                $geometry: { type: "Point",  coordinates: [ 31.287806, 34.238071 ] },
//                $minDistance: 100000,
//                $maxDistance: 500000
//              }
//           }
//       })
//       res.send(data);
// });


mongoose.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on("error", error => console.error(error));
mongoose.connection.once("open", () => console.log("mongo connect success"));

app.listen(process.env.PORT, () => console.log(`listening on port ${process.env.PORT}!`));