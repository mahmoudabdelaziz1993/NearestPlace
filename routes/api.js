const router = require("express").Router();
const Place = require('../model/Place');

// [ 30.07708, 31.285909 ]
router.post("/nearestlocations", async (req, res) => {
    const page = req.query.page || 1 ;
    const locationData = req.body.location.split(',')
    const long = parseFloat(locationData[0]);
    const lat = parseFloat(locationData[1]);
    const data = await Place.find({
        location:
        {
            $near:
            {
                $geometry: { type: "Point", coordinates: [long, lat] },
                $minDistance: 100000,
                $maxDistance: 500000
            }
        }
    },{_id:0})
    .limit(3)
    .skip(3*page)
    .sort({location : 'asc'})
    res.send(data);
});

module.exports = router;