const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PlaceSchema = new Schema({
    name:{type:String},
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            index: "2dsphere",
            required: true
        }
    }
});
PlaceSchema.index({ location: '2dsphere' });
module.exports = mongoose.model("Place", PlaceSchema);